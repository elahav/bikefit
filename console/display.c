/*
 * Copyright (C) 2020 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include "console.h"

static SDL_Window      *window;
static SDL_Renderer    *renderer;
static SDL_Texture     *background;
static TTF_Font        *font_large;
static TTF_Font        *font_small;

static char const *motor_text[] = {
    "Motor: Idle",
    "Motor: Down",
    "Motor: Up",
    "Motor: Failure"
};

/**
 * Helper function for drawing text using a given rectangle and colour.
 * @param   text    The text to draw
 * @param   x       Left side of the bounding rectangle
 * @param   y       Top side of the bounding rectangle
 * @param   width   Width of the bounding rectangle
 * @param   height  Height of the bounding rectangle
 * @param   red     Red component of the display colour
 * @param   green   Green component of the display colour
 * @param   blue    Blue component of the display colour
 */
static void
draw_text(
    char const * const  text,
    int const           x,
    int const           y,
    int const           width,
    int const           height,
    int const           red,
    int const           green,
    int const           blue
)
{
    SDL_Color	colour = { red, green, blue };
    SDL_Surface	*text_surface = TTF_RenderText_Solid(
        height >= 20 ? font_large : font_small,
        text, colour);
    SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, text_surface);

    SDL_Rect    src_rect = { 0, 0, text_surface->w, text_surface->h };
    SDL_Rect    dst_rect = { x, y, text_surface->w, text_surface->h };
    SDL_RenderCopy(renderer, texture, &src_rect, &dst_rect);

    SDL_FreeSurface(text_surface);
    SDL_DestroyTexture(texture);
}

/**
 * Draw the main window.
 * @param   info    Exercise information to display
 */
static void
draw_window(exercise_info_t const * const info)
{
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
    SDL_RenderClear(renderer);

    // Draw RPM image.
    SDL_Rect src_rect = { 0, 0, 500, 500 };
    SDL_Rect dst_rect = { 50, 50, 500, 500 };
    SDL_RenderCopy(renderer, background, &src_rect, &dst_rect);

    // Draw dial.
    double angle = ((-5.0 - info->sensor_info.speed) / 60.0) * 2 * M_PI;
    double x = 210 * sin(angle);
    double y = 210 * cos(angle);
    SDL_SetRenderDrawColor(renderer, 200, 0, 0, SDL_ALPHA_OPAQUE);
    SDL_RenderDrawLine(renderer, 300, 300, 300 + x, 300 + y);
    SDL_RenderDrawLine(renderer, 301, 301, 300 + x, 300 + y);

    char    buf[64];

    // Statistics.
    SDL_RenderDrawLine(renderer, 600, 40, 1000, 40);
    SDL_RenderDrawLine(renderer, 1000, 40, 1000, 300);
    SDL_RenderDrawLine(renderer, 600, 300, 1000, 300);
    SDL_RenderDrawLine(renderer, 600, 40, 600, 300);
    draw_text("Stats", 620, 30, 100, 18, 255, 255, 255);

    unsigned    time_sec = info->sensor_info.time_ms / 1000;
    snprintf(buf, sizeof(buf), "Time: %u:%.2u", time_sec / 60, time_sec % 60);
    draw_text(buf, 640, 60, 300, 20, 255, 255, 255);

    snprintf(buf, sizeof(buf), "Rotations: %d", info->sensor_info.rotations);
    draw_text(buf, 640, 120, 400, 20, 255, 255, 255);

    snprintf(buf, sizeof(buf), "RPM: %u", (unsigned)info->sensor_info.rpm);
    draw_text(buf, 640, 180, 400, 20, 255, 255, 255);

    snprintf(buf, sizeof(buf), "Dist.: %.1fKM",
             trunc(info->sensor_info.distance * 10.0) / 10.0);
    draw_text(buf, 640, 240, 400, 20, 255, 255, 255);

    SDL_RenderDrawLine(renderer, 600, 340, 1000, 340);
    SDL_RenderDrawLine(renderer, 1000, 340, 1000, 580);
    SDL_RenderDrawLine(renderer, 600, 580, 1000, 580);
    SDL_RenderDrawLine(renderer, 600, 340, 600, 580);
    draw_text("Program", 620, 330, 100, 18, 255, 255, 255);

    snprintf(buf, sizeof(buf), "%s", info->prog_name);
    draw_text(buf, 640, 360, 400, 20, 255, 255, 255);

    // Program information.
    if (info->prog_level != -1) {
        snprintf(buf, sizeof(buf), "Level: %d", info->prog_level);
        draw_text(buf, 640, 420, 400, 20, 255, 255, 255);

        time_sec = (unsigned)info->prog_time;
        snprintf(buf, sizeof(buf), "Time: %u:%.2u", time_sec / 60,
                 time_sec % 60);
        if (time_sec > 3) {
            draw_text(buf, 640, 480, 300, 20, 255, 255, 255);
        } else {
            draw_text(buf, 640, 480, 300, 20, 255, 165, 0);
        }
    }

    draw_text(motor_text[info->motor_state], 10, 540, 300, 18, 255, 255, 255);
    SDL_RenderPresent(renderer);
}

/**
 * Creates the program's window and associated SDL resources.
 */
bool
display_init()
{
    // Initialize.
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        fprintf(stderr, "SDL_Init: %s\n", SDL_GetError());
        return false;
    }

    if (TTF_Init() != 0) {
        fprintf(stderr, "TTF_Init: %s\n", SDL_GetError());
        return false;
    }

    font_large = TTF_OpenFont("./font.ttf", 36);
    if (font_large == NULL) {
        fprintf(stderr, "TTF_OpenFont: %s\n", SDL_GetError());
        return false;
    }

    font_small = TTF_OpenFont("./font.ttf", 18);
    if (font_small == NULL) {
        fprintf(stderr, "TTF_OpenFont: %s\n", SDL_GetError());
        return false;
    }

    // Create main window.
    SDL_CreateWindowAndRenderer(1024, 600, SDL_WINDOW_SHOWN, &window, &renderer);
    if (window == NULL) {
        fprintf(stderr, "SDL_CreateWindow: %s\n", SDL_GetError());
        return false;
    }

    // Create background texture.
    SDL_Surface * const background_surface = IMG_Load("./speedometer.png");
    background = SDL_CreateTextureFromSurface(renderer, background_surface);
    SDL_FreeSurface(background_surface);

    return true;
}

/**
 * Redraws the window
 * @param   info    Exercise information to display
 */
void
display_update(exercise_info_t const * const info)
{
    draw_window(info);
}
