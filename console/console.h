/*
 * Copyright (C) 2021 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CONSOLE_H
#define CONSOLE_H

#include <stdbool.h>
#include <stdint.h>
#include "bike_controller.h"

typedef struct
{
    bike_sensor_info_t  sensor_info;
    double              prog_time;
    int                 prog_level;
    char const         *prog_name;
    int                 motor_state;
} exercise_info_t;


typedef struct segment  segment_t;
typedef struct program  program_t;

/**
 * A segment in an exercise program.
 * Defines a duration and a difficulty level.
 */
struct segment
{
    segment_t  *next;
    int         level;
    unsigned    time;
    uint64_t    start;
};

/**
 * An exercise program, consisting of several segments.
 */
struct program
{
    program_t  *next;
    program_t  *prev;
    char const *name;
    segment_t  *head;
    segment_t  *cur;
    bool        active;
};

bool display_init(void);
void display_update(exercise_info_t const *);

bool controller_init(void);

void sensor_start(void);
void sensor_stop(void);
void sensor_read(exercise_info_t *);

void program_free(program_t *);
void program_dump(program_t *);
void program_start(program_t *, double *, int *);
bool program_update(program_t *, double *, int *);
void program_stop(program_t *);
bool program_load(program_t **);

bool buzzer_init(void);
void buzzer_update(unsigned);

bool motor_init(void);
int  motor_read(void);
void motor_down(void);
void motor_up(void);
void motor_set_level(int);
void motor_calibrate(void);

#endif
