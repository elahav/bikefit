/*
 * Copyright (C) 2021 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file    program.c
 * @brief   Support for user programs
 *
 * A program is a list of (level, time) pairs, referred to as segments. These
 * are stored in text files that are loaded and parsed by program_load().
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include <sys/neutrino.h>
#include <sys/syspage.h>
#include "console.h"

/**
 * Parses data from a program file.
 * The program file is expected to have one or more items of the format
 * LEVEL DELIMITER TIME DELIMITER
 * Where
 * - LEVEL is a number from 0 to 10
 * - DELIMITER is one of comma, space or newline
 * - TIME is the number of seconds
 * A '#' sign starts a comment in the file. Everything on a line following the
 * beginning of a comment is ignored.
 * @param   buf     Buffer holding the file's contents
 * @param   len     Buffer length
 * @param   program Program object to populate
 * @return  true if successful, false on parse error
 */
static bool
program_parse(char const * const buf, size_t const len,
              program_t * const program)
{
    // Parse.
    segment_t  *segment = NULL;
    segment_t **next_segment = &program->head;
    unsigned    value = 0;

    enum {
        INIT1   = 0x1,
        LEVEL   = 0x2,
        INIT2   = 0x3,
        TIME    = 0x4,
        COMMENT = 0x10
    }           state = INIT1;

    program->head = NULL;
    program->cur = NULL;

    for (ssize_t i = 0; i < len; i++) {
        if ((buf[i] >= '0') && (buf[i] <= '9')) {
            if (state == INIT1) {
                // Allocate a new segment.
                segment = malloc(sizeof(segment_t));
                if (segment == NULL) {
                    perror("malloc");
                    return false;
                }

                segment->next = NULL;
                state = LEVEL;
            } else if (state == INIT2) {
                state = TIME;
            }

            // Digit, add to value.
            if (__builtin_mul_overflow(value, 10, &value)) {
                fprintf(stderr, "Value too large %u\n", value);
                return false;
            }

            value += buf[i] - '0';
        } else if ((buf[i] == ',') || (buf[i] == ' ') || (buf[i] == '\n') ||
                   (buf[i] == '#')) {
            // Delimiter, next field.
            if (state == LEVEL) {
                if (value > 10) {
                    fprintf(stderr, "Level value too large %u\n", value);
                    return false;
                }

                segment->level = value;
                value = 0;
                state = INIT2;
            } else if (state == TIME) {
                segment->time = value;
                value = 0;
                state = INIT1;

                // Link to previous segment.
                // Also works for the first segment as a program head.
                *next_segment = segment;
                next_segment = &segment->next;
            } else {
                // Ignore multiple delimiters.
            }

            if (buf[i] == '#') {
                // Enter comment state.
                state |= COMMENT;
            }

            if (buf[i] == '\n') {
                state &= ~COMMENT;
            }
        } else if (state & COMMENT) {
            // Ignore everything after a comment marker.
        } else {
            fprintf(stderr, "Unexpected character '%c' in state %d\n",
                    buf[i], state);
            return false;
        }
    }

    if (program->head == NULL) {
        fprintf(stderr, "Empty program\n");
        return false;
    }

    return true;
}

/**
 * Loads a program file into a program_t object.
 * @param   path    The path of the file to parse
 * @param   program Program object to initialize
 * @return  true if successful, false on error
 */
static bool
program_load_one(int const dir_fd, program_t * const program)
{
    // Open the file.
    int const fd = openat(dir_fd, program->name, O_RDONLY);
    if (fd == -1) {
        perror("open");
        return false;
    }

    // Get file size.
    struct stat st;
    if (fstat(fd, &st) == -1) {
        perror("stat");
        close(fd);
        return false;
    }

    // Check for a reasonable size.
    if (st.st_size > 4096) {
        fprintf(stderr, "File too big: %zu\n", st.st_size);
        close(fd);
        return false;
    }

    // Allocate a buffer.
    char * const    buf = malloc(st.st_size + 1);
    if (buf == NULL) {
        perror("malloc");
        close(fd);
        return false;
    }

    // Read the file's contents.
    ssize_t const   len = read(fd, buf, st.st_size);
    if ((len < 0) || (len > st.st_size)) {
        perror("read");
        close(fd);
        free(buf);
    }

    close(fd);

    // Terminate the buffer to ensure the last segment is parsed.
    buf[len] = '\n';

    // Parse the data.
    bool const rc = program_parse(buf, len, program);
    free(buf);

    if (!rc) {
        program_free(program);
        return false;
    }

    return rc;
}

/**
 * Releases memory allocated to a program.
 * @param   program Program to free
 */
void
program_free(program_t * const program)
{
    segment_t   *next;
    for (segment_t *segment = program->head; segment != NULL; segment = next) {
        next = segment->next;
        free(segment);
    }
    program->head = NULL;
}

/**
 * Prints program information to the standard error stream.
 * @param   program Program to dump
 */
void
program_dump(program_t * const program)
{
    for (segment_t *segment = program->head; segment != NULL;
         segment = segment->next) {
        fprintf(stderr, "level=%u time=%u:%.2u\n", segment->level,
                segment->time / 60, segment->time % 60);
    }
}

/**
 * Start a program.
 * Starting a program sets the current segment indicator to the first segment
 * and records the current time.
 * @param       program Program to start
 * @param[out]  timep   Time left for the first segment
 * @param[out]  levelp  Initial level of the first segment
 */
void
program_start(program_t * const program, double * const timep,
              int * const levelp)
{
    program->cur = program->head;
    program->cur->start = ClockCycles();
    *timep = (double)program->cur->time;
    *levelp = program->cur->level;
}

/**
 * Checks and reports on the progress of the current program.
 * If the current segment is done then the program advances to the next one.
 * @param       program Program to start
 * @param[out]  timep   Time left for the current segment
 * @param[out]  levelp  Current level
 * @return  true if the program is still running, false otherwise
 */
bool
program_update(program_t * const program, double * const timep,
               int * const levelp)
{
    if (program->cur == NULL) {
        *timep = 0.0;
        *levelp = -1;
        return false;
    }

    uint64_t    now = ClockCycles();

    // Check how much time remains in the current segment.
    uint64_t    segment_cycles = now - program->cur->start;
    uint64_t    segment_seconds =
        segment_cycles / SYSPAGE_ENTRY(qtime)->cycles_per_sec;

    if (segment_seconds >= program->cur->time) {
        program->cur = program->cur->next;
        if (program->cur == NULL) {
            *timep = 0.0;
            *levelp = -1;
            buzzer_update(10);
            return false;
        }

        program->cur->start = now;
        segment_seconds = 0.0;
    }

    *timep = (double)(program->cur->time - segment_seconds);
    *levelp = program->cur->level;
    return true;
}

/**
 * Marks a program as no longer running.
 * @param       program Program to stop
 */
void
program_stop(program_t * const program)
{
    program->cur = NULL;
}

/**
 * Load all available programs.
 * Loads and parses every file under the programs directory, creating a linked
 * list of program_t objects.
 * @param   list    The program list to populate
 * @return  true if successful, false otherwise
 */
bool
program_load(program_t ** const list)
{
    DIR * const dir = opendir("./programs");
    if (dir == NULL) {
        perror("opendir");
        return false;
    }

    int const   dir_fd = dirfd(dir);

    program_t   *tail = *list;

    for (;;) {
        struct dirent * const ent = readdir(dir);
        if (ent == NULL) {
            break;
        }

        if (ent->d_name[0] == '.') {
            continue;
        }

#ifdef VARIANT_g
        fprintf(stderr, "%s\n", ent->d_name);
#endif

        program_t * const prog = calloc(1, sizeof(program_t));
        if (prog == NULL) {
            perror("calloc");
            closedir(dir);
            return false;
        }

        prog->name = strdup(ent->d_name);
        prog->prev = tail;
        if (tail != NULL) {
            tail->next = prog;
        } else {
            *list = prog;
        }
        tail = prog;

        if (!program_load_one(dir_fd, prog)) {
            closedir(dir);
            return false;
        }
    }

    closedir(dir);
    return true;
}
