/*
 * Copyright (C) 2020 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <fcntl.h>
#include <sys/neutrino.h>
#include <sys/iomsg.h>
#include "console.h"

static int ctrl_fd;

bool
controller_init(void)
{
    ctrl_fd = open("/dev/bike", O_RDWR);
    if (ctrl_fd == -1) {
        perror("Failed to connect to controller");
        return false;
    }

    return true;
}

/**
 * Change the buzzer state.
 * The function is invoked from the main loop whenever a program is running. It
 * is used to alert the user at the end of each segment that the level is about
 * to change.
 * @param   segment_time    Remaining time for the current segment, in seconds
 */
void
buzzer_update(unsigned const segment_time)
{
    static unsigned buzzer_times[] = { 200, 800, 200, 800, 400, 600, 0 };
    static unsigned buzzer_timer = 0;
    static int      buzzer_idx = -1;

    struct _io_msg  msg = {
        .type = _IO_MSG,
        .subtype = 0
    };

    // Update the buzzer if already running.
    if (buzzer_idx == -1) {
        // Buzzer timer is currently off, check if the remaining segment time is
        // under 3 seconds.
        if (segment_time <= 3) {
            // Start the buzzer.
            buzzer_idx = 0;
            buzzer_timer = buzzer_times[0];
            msg.subtype = BIKE_MSG_BUZZER_ON;
        }
    } else {
        // Buzzer is on, update time.
        buzzer_timer -= 20;
        if (buzzer_timer == 0) {
            buzzer_idx++;
            buzzer_timer = buzzer_times[buzzer_idx];
            if (buzzer_timer == 0) {
                // Buzzer done, turn off.
                buzzer_idx = -1;
                msg.subtype = BIKE_MSG_BUZZER_OFF;
            } else {
                // Move to next timer interval.
                msg.subtype = (buzzer_idx & 1) == 0 ?
                    BIKE_MSG_BUZZER_ON : BIKE_MSG_BUZZER_OFF;
            }
        }
    }

    if (msg.subtype != 0) {
        if (MsgSend(ctrl_fd, &msg, sizeof(msg), NULL, 0) == -1) {
            perror("MsgSend(BUZZER)");
        }
    }
}

void
sensor_start()
{
    struct _io_msg  msg = {
        .type = _IO_MSG,
        .subtype = BIKE_MSG_SENSOR_START
    };

    if (MsgSend(ctrl_fd, &msg, sizeof(msg), NULL, 0) == -1) {
        perror("MsgSend(SENSOR_START)");
    }
}

void
sensor_stop()
{
    struct _io_msg  msg = {
        .type = _IO_MSG,
        .subtype = BIKE_MSG_SENSOR_STOP
    };

    if (MsgSend(ctrl_fd, &msg, sizeof(msg), NULL, 0) == -1) {
        perror("MsgSend(SENSOR_STOP)");
    }
}

void
sensor_read(exercise_info_t * const infop)
{
    struct _io_msg  msg = {
        .type = _IO_MSG,
        .subtype = BIKE_MSG_SENSOR_READ
    };

    if (MsgSend(ctrl_fd, &msg, sizeof(msg), &infop->sensor_info,
                sizeof(bike_sensor_info_t)) == -1) {
        perror("MsgSend(SENSOR_READ)");
    }


#ifdef VARIANT_g
    fprintf(stderr, "rotations=%u time=%u rpm=%.2f speed=%.2f\n",
            infop->sensor_info.rotations, infop->sensor_info.time_ms,
            infop->sensor_info.rpm, infop->sensor_info.speed);
#endif
}

void
motor_up()
{
    struct _io_msg  msg = {
        .type = _IO_MSG,
        .subtype = BIKE_MSG_MOTOR_UP
    };

    if (MsgSend(ctrl_fd, &msg, sizeof(msg), NULL, 0) == -1) {
        perror("MsgSend(MOTOR_UP)");
    }
}

void
motor_down()
{
    struct _io_msg  msg = {
        .type = _IO_MSG,
        .subtype = BIKE_MSG_MOTOR_DOWN
    };

    if (MsgSend(ctrl_fd, &msg, sizeof(msg), NULL, 0) == -1) {
        perror("MsgSend(MOTOR_DOWN)");
    }
}

void
motor_set_level(int const level)
{
    if (level == -1) {
        return;
    }

    struct _io_msg  msg = {
        .type = _IO_MSG,
        .subtype = BIKE_MSG_MOTOR_SET_LEVEL,
        .mgrid = level
    };

    if (MsgSend(ctrl_fd, &msg, sizeof(msg), NULL, 0) == -1) {
        perror("MsgSend(MOTOR_SET_LEVEL)");
    }
}

int
motor_read()
{
    struct _io_msg  msg = {
        .type = _IO_MSG,
        .subtype = BIKE_MSG_MOTOR_STATE
    };

    int const   state = MsgSend(ctrl_fd, &msg, sizeof(msg), NULL, 0);
    if (state == -1) {
        perror("MsgSend(MOTOR_STATE)");
        return BIKE_MOTOR_FAIL;
    }

    return state;
}

void
motor_calibrate()
{
    struct _io_msg  msg = {
        .type = _IO_MSG,
        .subtype = BIKE_MSG_MOTOR_CALIBRATE
    };

    if (MsgSend(ctrl_fd, &msg, sizeof(msg), NULL, 0) == -1) {
        perror("MsgSend(MOTOR_DOWN)");
    }
}
