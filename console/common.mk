ifndef QCONFIG
QCONFIG=qconfig.mk
endif
include $(QCONFIG)

define PINFO
PINFO DESCRIPTION = Fitness program for a stationary bicyle
endef
INSTALLDIR=
NAME=bike_console
USEFILE=

EXTRA_INCVPATH=/home/elahav/src/sdl/nto/aarch64le/include ../../../controller/public
EXTRA_LIBVPATH=/home/elahav/src/sdl/nto/aarch64le/lib
LIBS=SDL2 SDL2_ttf SDL2_image m
#DEBUG=-g -O0

include $(MKFILES_ROOT)/qtargets.mk
