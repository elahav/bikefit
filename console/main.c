/*
 * Copyright (C) 2020 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <fcntl.h>
#include <SDL2/SDL.h>
#include "console.h"

/**
 * SDL event loop.
 * Polls and reacts to input events from the buttons.
 * @param   cur_prog    The currently selected program
 * @param   infop       Exercise information structure to fill if a program
 *                      starts or changes
 * @return  A newly selected program
 */
static program_t *
event_loop(program_t *cur_prog, exercise_info_t * const infop)
{
    SDL_Event   event;

    while (SDL_PollEvent(&event) != 0) {
        if (event.type == SDL_QUIT) {
            exit(EXIT_SUCCESS);
        }

        if (event.type == SDL_KEYDOWN) {
            switch (event.key.keysym.sym) {
            case SDLK_UP:
                // Start the current program.
                if (!cur_prog->active) {
                    sensor_start();
                    if (cur_prog->head != NULL) {
                        program_start(cur_prog, &infop->prog_time,
                                      &infop->prog_level);
                        infop->motor_state = motor_read();
                        display_update(infop);
                    }
                    cur_prog->active = true;
                }
                break;

            case SDLK_DOWN:
                // Stop the current program.
                if (cur_prog->active) {
                    sensor_stop();
                    program_stop(cur_prog);
                    cur_prog->active = false;
                }
                break;

            case SDLK_LEFT:
                if (cur_prog->active) {
                    // Manual motor control.
                    motor_down();
                } else {
                    // Switch to previous program, if the current program is
                    // not running.
                    if ((cur_prog->cur == NULL) && (cur_prog->prev != NULL)) {
                        cur_prog = cur_prog->prev;
                    }
                    infop->prog_name = cur_prog->name;
                }
                break;

            case SDLK_RIGHT:
                if (cur_prog->active) {
                    // Manual motor control.
                    motor_up();
                } else {
                    // Switch to next program, if the current program is not
                    // running.
                    if ((cur_prog->cur == NULL) && (cur_prog->next != NULL)) {
                        cur_prog = cur_prog->next;
                    }
                    infop->prog_name = cur_prog->name;
                }
                break;
            }
        }
    }

    return cur_prog;
}

/**
 * Main function.
 * Initializes the program and then runs the event and display loops.
 */
int
main(int argc, char **argv)
{
    if (!controller_init()) {
        return EXIT_FAILURE;
    }

    if (!display_init()) {
        return EXIT_FAILURE;
    }

    // Create a program list, starting with an empty one.
    program_t   empty_prog = { .name = "None" };
    program_t  *prog_list = &empty_prog;
    program_load(&prog_list);

    program_t   *cur_prog = prog_list;

    exercise_info_t info = { .prog_level = -1 };
    info.prog_name = cur_prog->name;
    sensor_read(&info);
    display_update(&info);

    motor_calibrate();

    unsigned    refresh = 0;

    for (;;) {
        cur_prog = event_loop(cur_prog, &info);

        SDL_Delay(20);
        refresh += 20;

        // Update program and display every 100ms.
        if (refresh >= 100) {
            if (cur_prog->head) {
                program_update(cur_prog, &info.prog_time, &info.prog_level);
                motor_set_level(info.prog_level);
            } else {
                info.prog_level = -1;
            }

            sensor_read(&info);
            info.motor_state = motor_read();
            display_update(&info);
            refresh = 0;
        }

        if (cur_prog->cur) {
            buzzer_update(info.prog_time);
        }
    }

    return 0;
}
