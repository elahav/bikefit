#include <fcntl.h>
#include <Python.h>

#include <sys/neutrino.h>
#include <sys/iomsg.h>
#include "bike_controller.h"

typedef struct constant constant_t;

/**
 * Constant published by the module.
 */
struct constant
{
    PyObject    *obj;
    char const  *name;
    unsigned    value;
};

static int          ctrl_fd;
static PyObject    *bike_err;

void
set_error(char const * const str, ...)
{
    static char err[256];
    va_list     args;

    va_start(args, str);
    vsnprintf(err, sizeof(err), str, args);
    va_end(args);

    PyErr_SetString(bike_err, err);
}

static PyObject *
sensor_start(PyObject *self, PyObject *args)
{
    struct _io_msg  msg = {
        .type = _IO_MSG,
        .subtype = BIKE_MSG_SENSOR_START
    };

    if (MsgSend(ctrl_fd, &msg, sizeof(msg), NULL, 0) == -1) {
        set_error("BIKE_MSG_SENSOR_START: %s", strerror(errno));
        return NULL;
    }

    Py_RETURN_NONE;
}

static PyObject *
sensor_stop(PyObject *self, PyObject *args)
{
    struct _io_msg  msg = {
        .type = _IO_MSG,
        .subtype = BIKE_MSG_SENSOR_STOP
    };

    if (MsgSend(ctrl_fd, &msg, sizeof(msg), NULL, 0) == -1) {
        set_error("BIKE_MSG_SENSOR_STOP: %s", strerror(errno));
        return NULL;
    }

    Py_RETURN_NONE;
}

static PyObject *
sensor_get_speed(PyObject *self, PyObject *args)
{
    struct _io_msg  msg = {
        .type = _IO_MSG,
        .subtype = BIKE_MSG_SENSOR_READ
    };

    bike_sensor_info_t  info;
    if (MsgSend(ctrl_fd, &msg, sizeof(msg), &info, sizeof(info)) == -1) {
        set_error("BIKE_MSG_SENSOR_READ: %s", strerror(errno));
        return NULL;
    }

    return PyFloat_FromDouble(info.speed);
}

static PyObject *
sensor_get_rotations(PyObject *self, PyObject *args)
{
    struct _io_msg  msg = {
        .type = _IO_MSG,
        .subtype = BIKE_MSG_SENSOR_READ
    };

    bike_sensor_info_t  info;
    if (MsgSend(ctrl_fd, &msg, sizeof(msg), &info, sizeof(info)) == -1) {
        set_error("BIKE_MSG_SENSOR_READ: %s", strerror(errno));
        return NULL;
    }

    return PyLong_FromLong(info.rotations);
}

static PyObject *
motor_up(PyObject *self, PyObject *args)
{
    struct _io_msg  msg = {
        .type = _IO_MSG,
        .subtype = BIKE_MSG_MOTOR_UP
    };

    if (MsgSend(ctrl_fd, &msg, sizeof(msg), NULL, 0) == -1) {
        set_error("BIKE_MSG_MOTOR_UP: %s", strerror(errno));
        return NULL;
    }

    Py_RETURN_NONE;
}

static PyObject *
motor_down(PyObject *self, PyObject *args)
{
    struct _io_msg  msg = {
        .type = _IO_MSG,
        .subtype = BIKE_MSG_MOTOR_DOWN
    };

    if (MsgSend(ctrl_fd, &msg, sizeof(msg), NULL, 0) == -1) {
        set_error("BIKE_MSG_MOTOR_DOWN: %s", strerror(errno));
        return NULL;
    }

    Py_RETURN_NONE;
}

static PyObject *
bike_controller_cleanup(PyObject *self, PyObject *args)
{
    close(ctrl_fd);
    Py_RETURN_NONE;
}

static PyMethodDef bike_methods[] = {
    {
        "sensor_start",
        (PyCFunction)sensor_start,
        METH_VARARGS,
        "Start the sensor operation."
    },
    {
        "sensor_stop",
        sensor_stop,
        METH_VARARGS,
        "Stop the sensor operation."
    },
    {
        "sensor_get_speed",
        sensor_get_speed,
        METH_VARARGS,
        "Read the current speed."
    },
    {
        "sensor_get_rotations",
        sensor_get_rotations,
        METH_VARARGS,
        "Read the number of rotations."
    },
    {
        "motor_up",
        motor_up,
        METH_VARARGS,
        "Turn the motor to reduce resistance."
    },
    {
        "motor_down",
        motor_down,
        METH_VARARGS,
        "Turn the motor to increase resistance."
    },
    {
        "cleanup",
        bike_controller_cleanup,
        METH_VARARGS,
        "Disconnect from the controller."
    },
    { NULL, NULL, 0, NULL }
};

static struct PyModuleDef   moduledef = {
    PyModuleDef_HEAD_INIT,
    "bike_controller",
    "Interface to an exercise bike controller",
    -1,
    bike_methods
};

PyMODINIT_FUNC
PyInit_bike_controller(void)
{
    // Establish a connection to the GPIO resource manager.
    ctrl_fd = open("/dev/bike", O_RDWR);
    if (ctrl_fd == -1) {
        return NULL;
    }

    // Register the module.
    PyObject * const    module = PyModule_Create(&moduledef);
    if (module == NULL) {
        return NULL;
    }

    bike_err = PyErr_NewException("bike_controller.error", NULL, NULL);
    Py_INCREF(bike_err);
    PyModule_AddObject(module, "error", bike_err);

    return module;
}
