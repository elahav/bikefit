ifndef QCONFIG
QCONFIG=qconfig.mk
endif
include $(QCONFIG)

define PINFO
PINFO DESCRIPTION = Python interface to an exercise bike controller
endef
INSTALLDIR=/usr/lib/python3.8
NAME=bike_controller
SONAME=bike_controller.so

include $(MKFILES_ROOT)/qtargets.mk
