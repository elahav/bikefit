/*
 * Copyright (C) 2021 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdatomic.h>
#include <sys/neutrino.h>
#include <sys/syspage.h>
#include <sys/rpi_gpio.h>
#include "bike.h"

/**
 * Whether the sensor is active.
 */
static bool             sensor_on = false;

/**
 * ClockCycles() rate.
 */
static double           cycles_per_msec;

/**
 * The cycle count when the sensor thread was last started.
 */
static uint64_t         start_cycles;

/**
 * Number of rotations since the sensor thread was started.
 */
static uint32_t         rotation_count;

/**
 * Number of items in rotation_records.
 */
#define ROTATION_BUF_SIZE   32

/**
 * Cicular buffer recording the number of cycles for each rotation, for the last
 * ROTATION_BUF_SIZE rotations.
 */
static struct {
    uint64_t    cycles;
    uint64_t    timestamp;
}                       rotation_records[ROTATION_BUF_SIZE];

/**
 * Index of last record added to rotation_records.
 */
static uint64_t         rotation_index;

/**
 * Configure the sensor pin for input.
 * @return  true if successful, false otherwise
 */
static bool
init_gpio(void)
{
    // Set GPIO as input.
    if (!gpio_select(GPIO_ROTATION_SENSOR, 0)) {
        return false;
    }

    // Set as pull up.
    rpi_gpio_msg_t  pud_msg = {
        .hdr.type = _IO_MSG,
        .hdr.subtype = RPI_GPIO_PUD,
        .hdr.mgrid = RPI_GPIO_IOMGR,
        .gpio = GPIO_ROTATION_SENSOR,
        .value = RPI_GPIO_PUD_UP
    };

    if (MsgSend(gpio_fd, &pud_msg, sizeof(pud_msg), NULL, 0) == -1) {
        perror("Failed to configure gpio pull up");
        return false;
    }

    return true;
}

/**
 * Get the current value of the sensor's pin.
 * @return 0/1 if successful, -1 otherwise
 */
static int
read_gpio(void)
{
    unsigned    value;
    if (!gpio_read(GPIO_ROTATION_SENSOR, &value)) {
        perror("Failed to read sensor");
        return -1;
    }

    return (int)value;
}

/**
 * Adds a record when a complete rotation is detected.
 * @param   cycles      The current time stamp
 * @param   prec_cycles Time stamp of the previous rotation
 */
static void
add_rotation_record(uint64_t const cycles, uint64_t const prev_cycles)
{
    unsigned const  idx = (rotation_index + 1) % ROTATION_BUF_SIZE;
    rotation_records[idx].cycles = cycles - prev_cycles;
    rotation_records[idx].timestamp = cycles;
    atomic_store_explicit(&rotation_index, idx, memory_order_release);
}

/**
 * Sensor reading thread.
 * The thread is created by sensor_init(), but does not run unless thread_flag
 * is true and thread_sem is posted.
 * thread_sem() is posted to. O
 * @param   arg     Ignored
 * @return  NULL
 */
void
sensor_tick(bool const first)
{
    static int      gpio_value = -1;
    static uint64_t prev_cycles = 0;

    if (first) {
        start_cycles = ClockCycles();
        prev_cycles = start_cycles;
        rotation_count = 0;
    }

    // Read the input.
    uint64_t const  cycles = ClockCycles();
    int const       value = read_gpio();
    if (value == 1) {
        gpio_value = 1;
        return;
    }

    // A value of 0 indicates that the pedals have completed one rotation.
    if (gpio_value == 1) {
        rotation_count++;
        add_rotation_record(cycles, prev_cycles);
        prev_cycles = cycles;
    }

    gpio_value = 0;
}

/**
 * Initializes the sensor module.
 * @return  true if successful, false otherwise
 */
bool
sensor_init()
{
    // General initialization.
    cycles_per_msec = SYSPAGE_ENTRY(qtime)->cycles_per_sec / 1000.0;

    if (!init_gpio()) {
        return false;
    }

    return true;
}

/**
 * Marks the sensor as active.
 * Calls to sensor_read() will return the latest values.
 */
void
sensor_start(void)
{
    sensor_on = true;
    ticker_start();
}

/**
 * Marks the sensor as inactive.
 * Calls to sensor_read() will return zeros for all values.
 */
void
sensor_stop(void)
{
    sensor_on = false;
    ticker_stop();
}

/**
 * Convert a cycle number into rotations per millisecond.
 * @param   cycles  Number of cycles to convert
 * @return  Equivalent rotations per millisecond
 */
static inline double
cycles_to_rpms(uint64_t const cycles)
{
    return cycles_per_msec / cycles;
}

/**
 * Calculate the speed in terms of rotations per minute, using a weighted
 * average of recent cycles per rotation values.
 * @param   cur_idx The index of the most recent rotation recording
 * @return  The calculated average
 */
static double
calculate_rpm(unsigned const cur_idx)
{
    uint64_t const  now = ClockCycles();
    uint64_t const  last_timestamp = rotation_records[cur_idx].timestamp;

    if ((now - last_timestamp) > (4000 * cycles_per_msec)) {
        // 4 seconds idle, consider as stopped.
        return 0;
    }

    // Calculate a weighted average.
    // avg = sum(i: 1 -> 5, x: idx -> idx-4){rpms(x) / (2^i)}
    unsigned    idx = (cur_idx - 4) % ROTATION_BUF_SIZE;
    double      avg_rpms = 0.0;
    for (unsigned i = 0; i < 5; i++) {
        if (rotation_records[idx].cycles > 0) {
            avg_rpms += cycles_to_rpms(rotation_records[idx].cycles);
        }
        avg_rpms /= 2.0;
        idx = (idx + 1) % ROTATION_BUF_SIZE;
    }

    uint64_t const  cycles_since_last = now - last_timestamp;

    if (cycles_since_last > rotation_records[cur_idx].cycles) {
        // Slowing down, but have not seen a rotatation yet.
        // Add the cycles since the last rotation to the average.
        avg_rpms = (avg_rpms + cycles_to_rpms(cycles_since_last)) / 2.0;
    }

    // Convert to rotations per minute.
    return avg_rpms * 60000.0;
}

/**
 * Get sensor information, using the latest information recorded by the sensor
 * thread.
 * @param   rcvid   Client's receive ID
 */
void
sensor_read(int const rcvid)
{
    bike_sensor_info_t  info = { 0 };

    if (sensor_on) {
        // Store total number of rotation and total time in milliseconds.
        // The acquire barrier ensures that the other values are up to date.
        unsigned const  idx = atomic_load_explicit(&rotation_index,
                                                   memory_order_acquire);
        info.rotations = rotation_count;
        info.time_ms = (ClockCycles() - start_cycles) / cycles_per_msec;

        // Get RPM.
        info.rpm = calculate_rpm(idx);

        // Pure speculation based on the readout of the original bike console:
        // Speed in KP/h is RPM/4.
        info.speed = info.rpm / 4.0;

        // By the same speculative calculation, every KM requires 240 rotations.
        info.distance = info.rotations / 240.0;
    }

    MsgReply(rcvid, 0, &info, sizeof(info));
}
