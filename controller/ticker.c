/*
 * Copyright (C) 2021 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/neutrino.h>
#include <sys/syspage.h>
#include "bike.h"

/**
 * true if the sensor reading thread is running, false otherwise.
 */
static bool             thread_flag;

/**
 * Semaphore used to kick the sensor thread.
 */
static sem_t            thread_sem;

/**
 * Time, in microseconds,  between sensor reads when the thread is running.
 */
static unsigned         thread_loop_time = 10000;

/**
 * Called by the sensor thread before proceeding to read the sensor's value.
 * If the thread is running then the function waits for the snapshot time.
 * If the thread is not running then the function waits on the semaphore.
 * @return  true if the thread has just started (woken up by the semaphore),
 *          false if the thread was woken up because it is time to take another
 *          reading
 */
static bool
wait_thread(void)
{
    if (thread_flag) {
        // Thread is running.
        // Sleep for the specified period.
        usleep(thread_loop_time);
        return false;
    } else {
        // Thread is not running.
        // Wait for the semaphore.
        sem_wait(&thread_sem);
        return true;
    }
}

/**
 * Ticker thread.
 * When active, calls the tick function for the different modules.
 */
static void *
ticker_thread(void * const arg)
{
    for (;;) {
        bool const  started = wait_thread();
        sensor_tick(started);
        motor_tick(thread_loop_time);
    }

    return NULL;
}

/**
 * Creates the ticker thread.
 * @return  true if successful, false otherwise
 */
bool
ticker_init(unsigned const priority)
{
    // General initialization.
    sem_init(&thread_sem, 0, 0);

    // Create a high-priority thread to read the input.
    pthread_attr_t      attr;
    struct sched_param  sched = { .sched_priority = priority };
    pthread_attr_init(&attr);
    pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setschedparam(&attr, &sched);

    int const   rc = pthread_create(NULL, &attr, ticker_thread, NULL);
    if (rc != 0) {
        fprintf(stderr, "pthread_create: %s\n", strerror(rc));
        return false;
    }

    return true;
}

/**
 * Start running the sensor thread.
 */
void
ticker_start(void)
{
    if (thread_flag) {
        return;
    }

    thread_flag = true;
    sem_post(&thread_sem);
}

/**
 * Stop the sensor thread.
 */
void
ticker_stop(void)
{
    if (thread_flag) {
        thread_flag = false;
    }
}
