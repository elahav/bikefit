/*
 * Copyright (C) 2021 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file    main.c
 * @brief   Fitness bike resource manager
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <sys/iofunc.h>
#include <sys/dispatch.h>
#include <sys/mman.h>
#include <sys/procmgr.h>
#include <sys/neutrino.h>
#include "bike.h"

static resmgr_connect_funcs_t   bike_connect_funcs;
static resmgr_io_funcs_t        bike_io_funcs;
static iofunc_attr_t            bike_io_attr;
static unsigned                 num_clients;
static unsigned                 verbose;

/**
 * Handles an _IO_CONNECT message.
 * This message can be sent for the directory, as a result of an opendir() call,
 * or to a particular node, from an open(), stat() or unlink() calls.
 * The function associates an open control block (OCB) object with the attribute
 * structure for the matching node. If no node exists and O_CREAT is specified,
 * a new one is created and added to the linked list of nodes.
 * @param   ctp     Message context
 * @param   msg     Connect message
 * @param   dirattr Pointer to the attribute structure for the directory
 * @param   extra   Opaque pointer from the resource manager library
 * @return  EOK if successful, error code otherwise
 */
static int
bike_open(
    resmgr_context_t * const    ctp,
    io_open_t * const           msg,
    iofunc_attr_t * const       dirattr,
    void * const                extra
)
{
    int rc;

    rc = iofunc_open_default(ctp, msg, dirattr, extra);
    if (rc != EOK) {
        return rc;
    }

    num_clients++;

    if (verbose >= 1) {
        printf("%s: %u\n", __func__, num_clients);
    }

    return EOK;
}

/**
 * Handles an _IO_MSG message.
 * @param   ctp     Message context
 * @param   msg     rpi_gpio_msg_t message
 * @param   ocb     Control block for the open file
 * @return  For input messages, EOK if successful, error code otherwise
 *          For output messages, _RESMGR_PTR if successful, error code otherwise
 */
static int
bike_msg(
    resmgr_context_t * const    ctp,
    io_msg_t * const            msg,
    iofunc_ocb_t * const        ocb
)
{
    int rc = _RESMGR_NOREPLY;

    // Make sure we have a complete message.
    // Note that the framework guarantees that at list sizeof(io_msg_t) is
    // available.
    switch (msg->i.subtype) {
    case BIKE_MSG_SENSOR_READ:
        sensor_read(ctp->rcvid);
        break;

    case BIKE_MSG_SENSOR_START:
        sensor_start();
        rc = EOK;
        break;

    case BIKE_MSG_SENSOR_STOP:
        sensor_stop();
        rc = EOK;
        break;

    case BIKE_MSG_MOTOR_UP:
        motor_up();
        rc = EOK;
        break;

    case BIKE_MSG_MOTOR_DOWN:
        motor_down();
        rc = EOK;
        break;

    case BIKE_MSG_MOTOR_SET_LEVEL:
        motor_set_level(msg->i.mgrid);
        rc = EOK;
        break;

    case BIKE_MSG_MOTOR_CALIBRATE:
        motor_calibrate();
        rc = EOK;
        break;

    case BIKE_MSG_MOTOR_STATE:
        MsgReply(ctp->rcvid, motor_read(), NULL, 0);
        break;

    case BIKE_MSG_BUZZER_ON:
        buzzer_toggle(true);
        rc = EOK;
        break;

    case BIKE_MSG_BUZZER_OFF:
        buzzer_toggle(false);
        rc = EOK;
        break;

    default:
        rc = ENOSYS;
        break;
    }

    return rc;
}

/**
 * Clean up when a process closes a file descriptor to the resource manager.
 * @param   ctp         Message context
 * @param   reserved    Unused
 * @param   vocb        Opaque open control block
 */
static int
bike_close_ocb(
    resmgr_context_t * const    ctp,
    void * const                reserved,
    iofunc_ocb_t * const        vocb
)
{
    num_clients--;
    if (num_clients == 0) {
        ticker_stop();
    }

    if (verbose >= 1) {
        printf("%s: %u\n", __func__, num_clients);
    }

    return 0;
}

/**
 * Handle an unblock pulse.
 * Releases the client.
 * @param   ctp     Message context
 * @param   pulse   Unblock pulse
 * @param   vocb    Opaque open control block
 */
static int
bike_unblock(
    resmgr_context_t * const    ctp,
    io_pulse_t * const          pulse,
    iofunc_ocb_t * const        vocb
)
{
    if (verbose >= 1) {
        printf("%s: %u\n", __func__, num_clients);
    }

    MsgError(ctp->rcvid, EINTR);
    return EOK;
}

/**
 * Main function.
 * Initializes the resource manager and then runs the message loop.
 */
int
main(int argc, char **argv)
{
    uid_t       uid = getuid();
    const char  *mount = "/dev/bike";
    unsigned    priority = 200;

    // Parse command-line options.
    for (;;) {
        int opt = getopt(argc, argv, "m:p:s:u:v");
        if (opt == -1) {
            break;
        } else if (opt == 'm') {
            mount = optarg;
        } else if (opt == 'p') {
            priority = strtoul(optarg, NULL, 0);
        } else if (opt == 'u') {
            uid = strtol(optarg, NULL, 0);
        } else if (opt == 'v') {
            verbose++;
        } else {
            fprintf(stderr, "Unknown option: '%c\n", opt);
            return 1;
        }
    }

    // Initialization.
    if (!gpio_init()) {
        return EXIT_FAILURE;
    }

    if (!ticker_init(priority)) {
        return EXIT_FAILURE;
    }

    if (!sensor_init()) {
        return EXIT_FAILURE;
    }

    if (!buzzer_init()) {
        return EXIT_FAILURE;
    }

    if (!motor_init(priority)) {
        return EXIT_FAILURE;
    }

    // Retain the ability to attach to the mount point before switching UID.
    if (procmgr_ability(0,
                        PROCMGR_ADN_NONROOT |
                        PROCMGR_AOP_ALLOW |
                        PROCMGR_AID_PATHSPACE,
                        PROCMGR_AID_EOL)
        != 0) {
        perror("procmgr_ability");
        return 1;
    }

    // Switch UID.
    if (setreuid(uid, uid) == -1) {
        perror("setreuid");
        return 1;
    }

    // Set up callback functions.
    iofunc_func_init(_RESMGR_CONNECT_NFUNCS, &bike_connect_funcs,
                     _RESMGR_IO_NFUNCS, &bike_io_funcs);

    bike_connect_funcs.open = bike_open;
    bike_io_funcs.msg = bike_msg;
    bike_io_funcs.close_ocb = bike_close_ocb;
    bike_io_funcs.unblock = bike_unblock;

    // Initialize node attributes.
    iofunc_attr_init(&bike_io_attr, S_IFREG | 0600, 0, 0);
    bike_io_attr.nbytes = 4096;

    // Initialize resource manager attributes.
    resmgr_attr_t       rattr;
    memset(&rattr, 0, sizeof(rattr));
    rattr.nparts_max = 1;
    rattr.msg_max_size = 2048;

    // Create the main dispatch structure.
    dispatch_t * const  dispatch =
        dispatch_create_channel(-1, DISPATCH_FLAG_NOLOCK);
    if (dispatch == NULL) {
        perror("dispatch_create");
        return EXIT_FAILURE;
    }

    // Attach to the mount point.
    int id = resmgr_attach(dispatch, &rattr, mount, _FTYPE_ANY, _RESMGR_FLAG_DIR,
                           &bike_connect_funcs, &bike_io_funcs,
                           &bike_io_attr);
    if (id < 0) {
        perror("resmgr_attach");
        return EXIT_FAILURE;
    }

    // Create the message context.
    dispatch_context_t  *ctx = dispatch_context_alloc(dispatch);
    if (ctx == NULL) {
        perror("dispatch_context_alloc");
        return EXIT_FAILURE;
    }

    // Message loop.
    for (;;) {
        if (dispatch_block(ctx) == NULL) {
            perror("dispatch_block");
            return EXIT_FAILURE;
        }

        dispatch_handler(ctx);
    }

    return 0;
}
