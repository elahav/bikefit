/*
 * Copyright (C) 2021 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file    bike.h
 * @brief   Internal header
 */

#ifndef BIKE_H
#define BIKE_H

#include <stdbool.h>
#include <stdint.h>
#include "public/bike_controller.h"

/**
 * GPIO assignments,
 */
enum
{
    GPIO_ROTATION_SENSOR    = 25,
    GPIO_BUZZER             = 9,
    GPIO_MOTOR_POWER        = 10,
    GPIO_MOTOR_DOWN         = 12,
    GPIO_MOTOR_UP           = 6,
    GPIO_IR_LED             = 1,
    GPIO_IR_RECEIVER        = 5
};

extern int gpio_fd;

bool gpio_init(void);
bool gpio_select(unsigned, unsigned);
bool gpio_set_pud(unsigned, unsigned);
bool gpio_write(unsigned, unsigned);
bool gpio_read(unsigned, unsigned *);

bool ticker_init(unsigned);
void ticker_start(void);
void ticker_stop(void);

bool sensor_init(void);
void sensor_tick(bool);
void sensor_read(int);
void sensor_start(void);
void sensor_stop(void);

bool buzzer_init(void);
void buzzer_toggle(bool);

bool motor_init(unsigned);
void motor_tick(unsigned);
int  motor_read(void);
void motor_down(void);
void motor_up(void);
void motor_set_level(int);
void motor_calibrate(void);

#endif
