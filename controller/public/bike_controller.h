/*
 * Copyright (C) 2021 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file    bike_controller.h
 * @brief   Public interface for the controller's resource manager
 */

#ifndef BIKE_CONTROLLER_H
#define BIKE_CONTROLLER_H

#include <stdint.h>

/**
 * Sensor information.
 */
typedef struct
{
    /** Time, in milliseconds, since the sensor was started. */
    uint32_t    time_ms;

    /** Number of rotations. */
    uint32_t    rotations;

    /** Apporximated distance, in metres. */
    double      distance;

    /** Rotations per minute. */
    double      rpm;

    /** Approximated speed, in kilometres per hour. */
    double      speed;
} bike_sensor_info_t;

/**
 * Message types.
 */
enum {
    BIKE_MSG_SENSOR_READ,
    BIKE_MSG_SENSOR_START,
    BIKE_MSG_SENSOR_STOP,
    BIKE_MSG_MOTOR_UP,
    BIKE_MSG_MOTOR_DOWN,
    BIKE_MSG_MOTOR_SET_LEVEL,
    BIKE_MSG_MOTOR_CALIBRATE,
    BIKE_MSG_MOTOR_STATE,
    BIKE_MSG_BUZZER_ON,
    BIKE_MSG_BUZZER_OFF
};

/**
 * Motor states.
 */
enum
{
    BIKE_MOTOR_IDLE,
    BIKE_MOTOR_DOWN,
    BIKE_MOTOR_UP,
    BIKE_MOTOR_FAIL
};

#endif
