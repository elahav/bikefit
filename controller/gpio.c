/*
 * Copyright (C) 2020 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <fcntl.h>
#include <sys/rpi_gpio.h>
#include "bike.h"

/**
 * Connection to the GPIO resource manager.
 */
int gpio_fd = -1;

/**
 * Connect to the GPIO resource manager.
 * @return  true if successful, false otherwise
 */
bool
gpio_init(void)
{
    // Connect to the GPIO resource manager.
    gpio_fd = open("/dev/gpio/msg", O_RDWR | O_CLOEXEC);
    if (gpio_fd == -1) {
        perror("open(/dev/gpio/msg)");
        return false;
    }

    return true;
}

/**
 * Set a GPIO's function.
 * @param   gpio    The number of the GPIO to configure
 * @param   func    The function number
 * @return  true if successful, false otherwise
 */
bool
gpio_select(unsigned const gpio, unsigned const func)
{
    rpi_gpio_msg_t  select_msg = {
        .hdr.type = _IO_MSG,
        .hdr.subtype = RPI_GPIO_SET_SELECT,
        .hdr.mgrid = RPI_GPIO_IOMGR,
        .gpio = gpio,
        .value = func
    };

    if (MsgSend(gpio_fd, &select_msg, sizeof(select_msg), NULL, 0) == -1) {
        perror("Failed to select GPIO function");
        return false;
    }

    return true;
}

/**
 * Set an input GPIO's pull resistor.
 * @param   gpio    The number of the GPIO to configure
 * @param   pull    Pull resistor value (off, up or down)
 * @return  true if successful, false otherwise
 */
bool
gpio_set_pud(unsigned const gpio, unsigned const pull)
{
    rpi_gpio_msg_t  pud_msg = {
        .hdr.type = _IO_MSG,
        .hdr.subtype = RPI_GPIO_PUD,
        .hdr.mgrid = RPI_GPIO_IOMGR,
        .gpio = gpio,
        .value = pull
    };

    if (MsgSend(gpio_fd, &pud_msg, sizeof(pud_msg), NULL, 0) == -1) {
        perror("Failed to set GPIO pull value");
        return false;
    }

    return true;
}

/**
 * Change the value of a GPIO output.
 * @param   gpio    The number of the GPIO to write to
 * @param   value   The new value to write
 * @return  true if successful, false otherwise
 */
bool
gpio_write(unsigned const gpio, unsigned const value)
{
    rpi_gpio_msg_t  output_msg = {
        .hdr.type = _IO_MSG,
        .hdr.subtype = RPI_GPIO_WRITE,
        .hdr.mgrid = RPI_GPIO_IOMGR,
        .gpio = gpio,
        .value = value
    };

    return MsgSend(gpio_fd, &output_msg, sizeof(output_msg), NULL, 0) != -1;
}

/**
 * Read the value of a GPIO input.
 * @param   gpio    The number of the GPIO to read from
 * @param   valuep  Holds the read value, on successful return
 * @return  true if successful, false otherwise
 */
bool
gpio_read(unsigned const gpio, unsigned * const valuep)
{
    rpi_gpio_msg_t  msg = {
        .hdr.type = _IO_MSG,
        .hdr.subtype = RPI_GPIO_READ,
        .hdr.mgrid = RPI_GPIO_IOMGR,
        .gpio = gpio
    };

    if (MsgSend(gpio_fd, &msg, sizeof(msg), &msg, sizeof(msg)) == -1) {
        return false;
    }

    *valuep = msg.value == 0 ? 0 : 1;
    return true;
}
