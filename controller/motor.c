/*
 * Copyright (C) 2020 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/neutrino.h>
#include <sys/syspage.h>
#include <sys/rpi_gpio.h>
#include "bike.h"

typedef struct command  command_t;

/**
 * Motor command structure.
 * Allows a linked list of commands to be queued from the main thread and
 * handled by the motor thread.
 */
struct command
{
    command_t  *next;
    int         command;
};

/**
 * Current state of the motor.
 */
static int              motor_state = BIKE_MOTOR_IDLE;

/**
 * Current motor level, corresponding to the last level indicated by the running
 * program.
 */
static int              motor_level = 0;

/**
 * Command queue.
 */
static struct {
    command_t      *head;
    command_t      *tail;
    pthread_mutex_t lock;
    pthread_cond_t  cond;
} command_queue = {
    .lock = PTHREAD_MUTEX_INITIALIZER,
    .cond = PTHREAD_COND_INITIALIZER
};

/**
 * Set the motor to turn such that the level screw goes down.
 */
static void
turn_down(void)
{
    if (gpio_write(GPIO_MOTOR_DOWN, 1)) {
        motor_state = BIKE_MOTOR_DOWN;
    } else {
        perror("Failed to set gpio value (motor up)");
        motor_state = BIKE_MOTOR_FAIL;
    }
}

/**
 * Set the motor to turn such that the level screw goes up.
 */
static void
turn_up(void)
{
    if (gpio_write(GPIO_MOTOR_UP, 1)) {
        motor_state = BIKE_MOTOR_UP;
    } else {
        perror("Failed to set gpio value (motor up)");
        motor_state = BIKE_MOTOR_FAIL;
    }
}

/**
 * Turn off the motor.
 */
static void
turn_off(void)
{
    if (!gpio_write(GPIO_MOTOR_DOWN, 0)) {
        perror("Failed to set gpio value (motor down)");
        motor_state = BIKE_MOTOR_FAIL;
    }

    if (!gpio_write(GPIO_MOTOR_UP, 0)) {
        perror("Failed to set gpio value (motor up)");
        motor_state = BIKE_MOTOR_FAIL;
    }

    if (motor_state != BIKE_MOTOR_FAIL) {
        motor_state = BIKE_MOTOR_IDLE;
    }
}

/**
 * Read the IR receiver GPIO to see if it is detecting a complete half-turn.
 * @return  true if the receiver detects a high signal, false if the receiver
 *          detects a low signal or gpio_read() fails
 */
static bool
read_ir_receiver(void)
{
    unsigned    value;
    if (!gpio_read(GPIO_IR_RECEIVER, &value)) {
        perror("Failed to read IR receiver");
        return false;
    }

    return value == 1;
}

/**
 * Remove a command from the head of the command queue.
 * @return  The removed command, or NULL if the queue is empty.
 */
static command_t *
dequeue_command(void)
{
    if (command_queue.head == NULL) {
        return NULL;
    }

    // Dequeue a command.
    pthread_mutex_lock(&command_queue.lock);
    command_t * const cmd = command_queue.head;
    command_queue.head = cmd->next;
    if (command_queue.head == NULL) {
        command_queue.tail = NULL;
    }
    pthread_mutex_unlock(&command_queue.lock);

    return cmd;
}

/**
 * Tick handler for the motor.
 * Acts on the current motor command.
 * @param   tick_ms     The duration of the tick, in milliseconds
 */
void
motor_tick(unsigned const tick_ms)
{
    static command_t   *cur_cmd = NULL;
    static unsigned     delay_ms = 0;
    static unsigned     move_ms = 0;

    // If not running a command, check if one is available in the queue.
    if (cur_cmd == NULL) {
        cur_cmd = dequeue_command();
        if (cur_cmd == NULL) {
            // Nothing to do.
            return;
        }

        // New command found.
        // Turn on the LED.
        gpio_write(GPIO_IR_LED, 1);

        // Turn on motor driver.
        gpio_write(GPIO_MOTOR_POWER, 1);

        // Turn the motor in the right direction.
        if (cur_cmd->command == BIKE_MOTOR_UP) {
            turn_up();
        } else {
            turn_down();
        }

        // Give the motor some time to spin so that it blocks the IR LED.
        delay_ms = 500;
        return;
    }

    // Already executing a command.
    move_ms += 10;

    // Check if waiting for a delay to finish.
    if (delay_ms != 0) {
        delay_ms -= 10;
        return;
    }

    // Check the IR receiver.
    if (!read_ir_receiver()) {
        if (move_ms < 4000) {
            // Keep going.
            return;
        }

        // Haven't detected the IR LED for 4 seconds, someting went wrong.
        motor_state = BIKE_MOTOR_FAIL;
        fprintf(stderr, "Failed to detect motor stop signal\n");
    }

    // Turn off motor driver.
    gpio_write(GPIO_MOTOR_POWER, 0);

    // Turn off the LED.
    gpio_write(GPIO_IR_LED, 0);

    // Turn off the motors.
    turn_off();

    free(cur_cmd);
    cur_cmd = NULL;
    move_ms = 0;
}

/**
 * When the RPi starts it briefly turns on some GPIOs, causing the motor to
 * spin.
 * Try to go back to the initial level.
 */
void
motor_calibrate(void)
{
    // Turn on the LED.
    gpio_write(GPIO_IR_LED, 1);

    // Turn on motor driver.
    gpio_write(GPIO_MOTOR_POWER, 1);

    // Turn the motor down.
    turn_down();

    printf("%s: down\n", __func__);

    // Wait 100ms for detection.
    bool done = false;
    for (unsigned i = 0; i < 100; i++) {
        if (read_ir_receiver()) {
            done = true;
            break;
        }

        usleep(10000);
    }

    printf("%s: off (%d)\n", __func__, done);
    turn_off();

    if (!done) {
        // Initial state not found gowing down, try going up for twice as long.
        printf("%s: up\n", __func__);
        turn_up();

        // Wait 200ms for detection.
        for (unsigned i = 0; i < 200; i++) {
            if (read_ir_receiver()) {
                done = true;
                break;
            }

            usleep(10000);
        }

        printf("%s: off (%d)\n", __func__, done);
        turn_off();
    }

    // Turn off motor driver.
    gpio_write(GPIO_MOTOR_POWER, 0);

    // Turn off the LED.
    gpio_write(GPIO_IR_LED, 0);
}

/**
 * Configure the motor GPIOs and start the motor control thread.
 * @return  true if successful, false otherwise
 */
bool
motor_init(unsigned const priority)
{
    // Set motor GPIOs as outputs;
    if (!gpio_select(GPIO_MOTOR_POWER, 1)) {
        return false;
    }

    if (!gpio_select(GPIO_MOTOR_DOWN, 1)) {
        return false;
    }

    if (!gpio_select(GPIO_MOTOR_UP, 1)) {
        return false;
    }

    // Set IR LED GPIO as output.
    if (!gpio_select(GPIO_IR_LED, 1)) {
        return false;
    }

    // Set IR receiver GPIO as pull-down input.
    if (!gpio_select(GPIO_IR_RECEIVER, 0)) {
        return false;
    }

    if (!gpio_set_pud(GPIO_IR_RECEIVER, RPI_GPIO_PUD_DOWN)) {
        return false;
    }

    // Turn off motor GPIOs.
    gpio_write(GPIO_MOTOR_POWER, 0);
    gpio_write(GPIO_MOTOR_DOWN, 0);
    gpio_write(GPIO_MOTOR_UP, 0);

    return true;
}

/**
 * Report the current state of the motor.
 * The information may be stale, but is only used for display purposes.
 * @return  One of the BIKE_MOTOR_* constants
 */
int
motor_read()
{
    return motor_state;
}

/**
 * Enqueue a BIKE_MOTOR_DOWN command for the motor controlling thread.
 */
void
motor_down()
{
    command_t * const   cmd = malloc(sizeof(command_t));
    cmd->command = BIKE_MOTOR_DOWN;

    pthread_mutex_lock(&command_queue.lock);
    cmd->next = NULL;
    if (command_queue.tail) {
        command_queue.tail->next = cmd;
    } else {
        command_queue.head = cmd;
        pthread_cond_signal(&command_queue.cond);
    }
    command_queue.tail = cmd;
    pthread_mutex_unlock(&command_queue.lock);
}

/**
 * Enqueue a BIKE_MOTOR_UP command for the motor controlling thread.
 */
void
motor_up()
{
    command_t * const   cmd = malloc(sizeof(command_t));
    cmd->command = BIKE_MOTOR_UP;

    pthread_mutex_lock(&command_queue.lock);
    cmd->next = NULL;
    if (command_queue.tail) {
        command_queue.tail->next = cmd;
    } else {
        command_queue.head = cmd;
        pthread_cond_signal(&command_queue.cond);
    }
    command_queue.tail = cmd;
    pthread_mutex_unlock(&command_queue.lock);
}

/**
 * Adjust the motor to the given level by enqueuing as many of the necessary
 * commands to get from the current level to the new one.
 * @param   new_level   The new level to get to
 */
void
motor_set_level(int const new_level)
{
    if (new_level == motor_level) {
        return;
    }

    if (new_level == -1) {
        return;
    }

    if (new_level > motor_level) {
        // New level is higher, go down.
        for (int i = motor_level; i < new_level; i++) {
            motor_down();
        }
    } else {
        // New level is lower, go down.
        for (int i = new_level; i < motor_level; i++) {
            motor_up();
        }
    }

    motor_level = new_level;
}
